#!usr/bin/env python3
# -*- coding:utf-8 -*-

def mul():
    list=[]
    for m in range(0,10):
        for n in range(1,m+1):
            list.append([n,m,m*n])
    return list

it=iter(list(mul()))

def td(number,i):
    """
    生成行
    number:行内数字
    i:行长度
    """
    l=""
    for a in range(i):
        l=l+"<td><br>"+number+"<br></td>"
    return l

def tr(text):
    """
    生成列
    text:生成行返回值
    """
    return "<tr><br>"+text+"<br></tr>"

for x in range(10):
    for b in mul():
        ls=next(it)
        s=str(ls[0])+"*"+str(ls[1])+"="+str(ls[2])
        print(tr(td(s,x)))

